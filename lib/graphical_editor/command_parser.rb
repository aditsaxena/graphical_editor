# Example of conversion:
#
# 'I 10 20'   => [:init_matrix, [10, 20]]
# 'L 10 20 R' => [:light_pixel_with_colour, [10, 20, 'R']]
# 'X'         => [:exit]

module GraphicalEditor

  COMMANDS = {
    init_matrix: 'I',
    light_pixel_with_colour: 'L',
    v_line_with_colour: 'V',
    h_line_with_colour: 'H',
    fill_region: 'F',
    show_image: 'S',
    clear_table: 'C',
    exit: 'X',
  }
  
  class CommandParser

    def parse_command(raw_command)
      command, *params = raw_command.split(' ')
      
      command = convert_command(command)
      params = convert_ints_in_params(params)

      [command, params]
    end

    private

      def convert_command(command)
        COMMANDS.invert[command.to_s.upcase]
      end

      def convert_ints_in_params(params)
        params.map { |param| Integer(param) rescue param }
      end

  end

end
