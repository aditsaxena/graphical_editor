module GraphicalEditor

  class InteractiveSession
    
    def initialize(inputter: ::Readline, displayer: Displayer.new, cg_engine: CgEngine.new, parser: CommandParser.new)
      @inputter = inputter

      while true
        command, params = parser.parse_command(read_input)
        return if command == :exit

        output = cg_engine.elaborate command, params
        
        displayer.show output
      end
    end

    def read_input
      @inputter.readline("> ", history = true)
    end

  end
end
