module GraphicalEditor

  class Displayer

    def initialize(outputter: 'puts', format: "\n=>\n%{output}")
      @outputter = outputter
      @format = format
    end

    def show(output)
      formatted_output = @format % { output: output }

      if @outputter.is_a? String
        ::Object.send @outputter, formatted_output
      else
        @outputter.call formatted_output
      end
    end

  end
  
end
