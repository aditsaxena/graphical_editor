module GraphicalEditor

  class CgEngine

    attr_reader :matrix

    def initialize
      @matrix = []
    end

    def initialized?
      ! @matrix.empty?
    end

    def init_matrix(n, m)
      @matrix = m.times.collect { Array.new(n, 0) }
      nil
    end

    def light_pixel_with_colour(x, y, c)
      x = x - 1
      y = y - 1
      @matrix[y][x] = c
      nil
    end

    def v_line_with_colour(x, y1, y2, c)
      (y1..y2).each do |y|
        light_pixel_with_colour x, y, c
      end
      nil
    end

    def h_line_with_colour(x1, x2, y, c)
      (x1..x2).each do |x|
        light_pixel_with_colour x, y, c
      end
      nil
    end

    def fill_region(x, y, c)
      x = x - 1
      y = y - 1
      filler x, y, @matrix[y][x], c
      nil
    end

    def show_image
      @matrix.map { |row| row.join('') }.join("\n")
    end

    def clear_table
      init_matrix @matrix.first.length, @matrix.length
      nil
    end

    def elaborate(command, params)
      return "Invalid command" unless valid_command? command, params
      send command, *params
    end

    def valid_command?(command, params)
      return false if command != :init_matrix && ! initialized?

      case command
      when :init_matrix
        params.length == 2 &&
        params[0] > 0 && params[1] > 0

      when :light_pixel_with_colour
        w, h, c = params

        w && h && c &&
        w.is_a?(Numeric) && h.is_a?(Numeric) &&
        w.between?(1, max_w) &&
        h.between?(1, max_h)

      when :v_line_with_colour
        w, h1, h2, c = params

        w && h1 && h2 && c &&
        w.is_a?(Numeric) && h1.is_a?(Numeric) && h2.is_a?(Numeric) &&
        w.between?(1, max_w) &&
        h1.between?(1, max_h) &&
        h2.between?(1, max_h) &&
        h1 <= h2

      when :h_line_with_colour
        w1, w2, h, c = params

        w1 && w2 && h && c &&
        w1.is_a?(Numeric) && w2.is_a?(Numeric) && h.is_a?(Numeric) &&
        w1.between?(1, max_w) &&
        w2.between?(1, max_w) &&
        w1 <= w2 &&
        h.between?(1, max_h)

      when :fill_region
        w, h, c = params
        
        w && h && c &&
        w.is_a?(Numeric) && h.is_a?(Numeric) &&
        w.between?(1, max_w) &&
        h.between?(1, max_h)

      when :show_image
        true

      when :clear_table
        true

      else
        false

      end
    end

    private

      def max_w
        initialized? ? @matrix.first.length : nil
      end

      def max_h
        initialized? ? @matrix.length : nil
      end
      
      # http://rubyquiz.strd6.com/quizzes/201-flood-fill-visualization
      def filler(x, y, target_color, replacement_color)
        return unless (@matrix[y][x] rescue nil) && x >= 0 && y >= 0 # valid point?
        return if @matrix[y][x] != target_color
        return if @matrix[y][x] == replacement_color
        
        @matrix[y][x] = replacement_color
        filler(x + 1, y, target_color, replacement_color)
        filler(x - 1, y, target_color, replacement_color)
        filler(x, y + 1, target_color, replacement_color)
        filler(x, y - 1, target_color, replacement_color)  
      end

  end

end
