require 'spec_helper'

describe GraphicalEditor::CommandParser do

  subject { GraphicalEditor::CommandParser.new }

  before do
  end

  context ".parse_command" do
    it 'parses a command' do
      expect(subject.parse_command "l 2 1 r").to eq [:light_pixel_with_colour, [2, 1, 'r']]
    end
    
    it 'parses any command (no validation)' do
      GraphicalEditor::COMMANDS.each do |command_full, command_short|
        expect(subject.parse_command "#{command_short} 2 r 2").to eq [command_full, [2, 'r', 2]]
      end
    end
    
  end

end
