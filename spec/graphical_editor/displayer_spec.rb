require 'spec_helper'

describe GraphicalEditor::Displayer do

  subject { GraphicalEditor::Displayer }

  before do
    @outputter = double
    @displayer = subject.new outputter: @outputter
  end

  it '.show' do
    @outputter.should_receive(:call).with "\n=>\nhello"
    @displayer.show 'hello'
  end

  context 'request' do

    it '.show with format' do
      @displayer = subject.new outputter: @outputter, format: "\n\n\nOutput: %{output}"
      @outputter.should_receive(:call).with "\n\n\nOutput: hello"
      @displayer.show 'hello'
    end

  end

end
