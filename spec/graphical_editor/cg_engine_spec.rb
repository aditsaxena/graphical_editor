require 'spec_helper'

describe GraphicalEditor::CgEngine do

  subject { GraphicalEditor::CgEngine.new }

  it '.init_matrix' do
    expect(subject.matrix).to eq []

    subject.init_matrix 5, 6

    expect(subject.matrix).to eq [
      [0,0,0,0,0],
      [0,0,0,0,0],
      [0,0,0,0,0],
      [0,0,0,0,0],
      [0,0,0,0,0],
      [0,0,0,0,0],
    ]
  end

  it '.initialized?' do
    expect(subject.initialized?).to eq false
    subject.init_matrix 5, 6
    expect(subject.initialized?).to eq true
  end

  context '.valid_command?' do
    let(:valid_commands) {
      {
        init_matrix: [5, 6],
        light_pixel_with_colour: [2, 6, 'r'],
        v_line_with_colour: [2, 5, 6, 'r'],
        h_line_with_colour: [1, 5, 2, 'r'],
        fill_region: [2, 2, 'f'],
        show_image: nil,
        clear_table: nil,
      }
    }
    it 'allows draw commands only if initialized' do
      valid_commands.each do |command, params|

        if command == :init_matrix
          expect(subject.valid_command? command, params).to be_truthy
        else
          expect(subject.valid_command? command, params).to be_falsey
        end

      end
    end

    context 'initialized matrix' do

      before do
        subject.init_matrix 5, 6
      end

      it "for light_pixel_with_colour && fill_region" do
        [:light_pixel_with_colour, :fill_region].each do |command|
          expect(subject.valid_command? command, []).to be_falsey
          expect(subject.valid_command? command, [1, 7, 'r']).to be_falsey # out-of-bounds
          expect(subject.valid_command? command, [6, 1, 'r']).to be_falsey # out-of-bounds
          expect(subject.valid_command? command, [nil, nil, 'f']).to be_falsey
          expect(subject.valid_command? command, [5, 6, nil]).to be_falsey
          expect(subject.valid_command? command, [5, 6, 'r']).to be_truthy
        end
      end

      it "for v_line_with_colour" do
        expect(subject.valid_command? :v_line_with_colour, [5, 0, 0, 'r']).to be_falsey
        expect(subject.valid_command? :v_line_with_colour, [5, 5, 7, 'r']).to be_falsey
        expect(subject.valid_command? :v_line_with_colour, [5, 7, 6, 'r']).to be_falsey
        expect(subject.valid_command? :v_line_with_colour, [5, 1, 6, nil]).to be_falsey
        expect(subject.valid_command? :v_line_with_colour, [5, 1, 6, 'r']).to be_truthy
      end

      it "for h_line_with_colour" do
        expect(subject.valid_command? :h_line_with_colour, [1, 0, 6, 'r']).to be_falsey
        expect(subject.valid_command? :h_line_with_colour, [1, 6, 6, 'r']).to be_falsey
        expect(subject.valid_command? :h_line_with_colour, [1, 5, 7, 'r']).to be_falsey
        expect(subject.valid_command? :h_line_with_colour, [1, 5, 6, nil]).to be_falsey
        expect(subject.valid_command? :h_line_with_colour, [1, 5, 6, 'r']).to be_truthy
      end

    end

  end

  context 'initialized matrix' do
    
    before do
      subject.init_matrix 5, 6
    end

    it ".show_image" do
      expect(subject.show_image).to eq(
"00000
00000
00000
00000
00000
00000")
    end

    it ".light_pixel_with_colour" do
      subject.light_pixel_with_colour 3, 2, 'W'
      expect(subject.show_image).to eq(
"00000
00W00
00000
00000
00000
00000")
    end

    it ".v_line_with_colour" do
      subject.v_line_with_colour 2, 2, 5, 'W'
      subject.v_line_with_colour 3, 3, 6, 'F'
      expect(subject.show_image).to eq(
"00000
0W000
0WF00
0WF00
0WF00
00F00")
    end

    it ".h_line_with_colour" do
      subject.h_line_with_colour 2, 5, 3, 'W'
      subject.h_line_with_colour 1, 4, 5, 'F'
      expect(subject.show_image).to eq(
"00000
00000
0WWWW
00000
FFFF0
00000")
    end

    it ".fill_region" do
      subject.light_pixel_with_colour 2, 1, 'r'
      subject.light_pixel_with_colour 2, 2, 'r'
      subject.light_pixel_with_colour 3, 3, 'r'
      subject.light_pixel_with_colour 4, 4, 'r'
      subject.light_pixel_with_colour 4, 5, 'r'
      subject.light_pixel_with_colour 5, 5, 'r'
      subject.fill_region 3, 1, '-'

      expect(subject.show_image).to eq(
"0r---
0r---
00r--
000r-
000rr
00000")
    end

    it ".clear_table" do
      subject.h_line_with_colour 2, 5, 3, 'W'
      expect(subject.show_image).to eq(
"00000
00000
0WWWW
00000
00000
00000")

      subject.clear_table

      expect(subject.show_image).to eq(
"00000
00000
00000
00000
00000
00000")
    end

  end

end
