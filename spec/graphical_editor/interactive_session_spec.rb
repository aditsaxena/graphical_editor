require 'spec_helper'

describe GraphicalEditor::InteractiveSession do

  let(:inputter) { double('Inputter') }
  let(:displayer) { double('displayer').tap { |s| s.stub(:show) } } # eg. { Proc.new {|n| puts n } }

  before do
  end

  it 'mocks the inputter' do
    inputter.stub(:readline).and_return('X')

    GraphicalEditor::InteractiveSession.new inputter: inputter

    expect(inputter).to have_received(:readline).exactly(1).times
  end

  context 'executing a series of commands' do

    before do
      inputter.stub(:readline).and_return('I 5 6', 'S', 'X')
    end

    it 'basic' do
      GraphicalEditor::InteractiveSession.new inputter: inputter
      expect(inputter).to have_received(:readline).exactly(3).times
    end

    it 'mocks the output' do
      displayer.stub :call
      GraphicalEditor::InteractiveSession.new inputter: inputter, displayer: displayer
      expect(displayer).to have_received(:show).exactly(2).times
    end
    
  end

end
