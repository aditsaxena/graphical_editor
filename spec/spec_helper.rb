require 'awesome_print'
require 'pry'
require './lib/graphical_editor.rb'

RSpec.configure do |config|
  
  config.filter_run :focus => true
  config.run_all_when_everything_filtered = true

  config.order = "random" # --seed 1234
  
  config.before(:suite) do
  end

  config.before(:each) do
  end

end
